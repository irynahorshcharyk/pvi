class searchcontroller < ApplicationController 

    def show
      @tutorials = Tutorial.search(params[:q])
  
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @tutorial }
      end
    end
  end